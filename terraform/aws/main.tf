resource "aws_key_pair" "deployer" {
  key_name   = "keyfile.pub"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDJS+wfphQUGCaaYAKCY47xy2Jhy/JmgpA8h2T5Z289AT5gqvUx13qMqeAXKXpAAtzSWFJqqrSiyyhXtVTET91Tzsu0keOmaEhZ7S6SYVdThVsu5xX7B7LUI1juLjZn6RcTm24a0vhEUK9E9Oq1/kiccsms3Vv5++KYgvMhDbniyGpkP8ibkBnppNeLY7U0UU1puh8OujCrTczuHx5p0kjGiJ/k0JoOmF8AHysz0msdM2Eq7/U8O1Pyu+hAp8ajnbp94J5218shS2VBOu85/3gEjxWrgOmsWmyxQ5gbOaqPhzw9K2oQAnniGC9Fisv3EXU0+3YVly0gGorRv5at9KU3OXYolhZDoPfAm6u1+ijD90e+OVLpdZYAPu9La0Mxdu8EVF8bTBI65oQbMqPqzixCqCcoPu0pjaCbbfS9i9EpeJDtaLjNJAIlUrga/yHB84mU2W7TVYA77ZoLUrFC+bsy1oourb3Sq5qdXxYqbWYHRcMnycRxDm92X2LUmMp1dW8= root@LAPTOP-QM7QNA3A"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_vpc" "mainvpc" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.mainvpc.id

  ingress {
    protocol  = "tcp"
    self      = true
    from_port = 0
    to_port   = 22
  }

  ingress {
    protocol  = "tcp"
    self      = true
    from_port = 0
    to_port   = 80
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}